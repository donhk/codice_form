@extends('layouts.app')

@section('content')
    <section id="contenedorPago">
        <div class="container">
            <div class="cajaInformacion">
                <div class="contenedorConfirmacion">
                    <h1>it was not possible to process your txn :(</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
