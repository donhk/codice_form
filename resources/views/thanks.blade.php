@extends('layouts.app')

@section('content')
    <section id="contenedorPago">
        <div class="container">
            <div class="cajaInformacion">
                <div class="contenedorConfirmacion">
                    <img src="{{asset('img/logo2.png')}}" alt="">
                    <h1>Gracias por tu compra</h1>
                    @if($message = \Session::get('paypal_payment_id'))
                        {{$payment_id}}
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
