@extends('layouts.app')

@section('content')
    <section id="contenedorPago">
        <div class="container">
            <div class="cajaInformacion">
                <div class="contenedorConfirmacion">
                    <img src="{{asset('img/logo2.png')}}" alt="">
                    <h1>Formulario de pago</h1>
                    <img src="{{asset('img/tarjetas.png')}}" alt="" width="100%;">
                    <br>
                    <form id="my_form" action="{{route('paypal')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Elegir producto</label><br>
                            <select name="product" class="form-control" onchange="refresh(this);" id="select-opt">
                                @foreach($keys as $key)
                                    <option value="{{$key}}">{{$items[$key]}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Precio</label>
                            <input type="text" name="price" class="form-control" id="price" placeholder="$1890.00"
                                   disabled>
                        </div>
                        <div class="form-group">
                            <label for="">Numero de tarjeta</label>
                            <input type="number" name="card_number" class="form-control" id="card_number">
                        </div>
                        <div class="form-group">
                            <label for="">Nombre del titular de la tarjeta</label>
                            <input type="text" name="card_holder" class="form-control" id="card_holder">
                        </div>
                        <div class="form-group">
                            <label for="">Apellidos del titular de la tarjeta</label>
                            <input type="text" name="card_surnames" class="form-control" id="card_surnames">
                        </div>
                        <div class="form-group">
                            <label for="">Vencimiento</label>
                            <select name="expire_month" id="expire_month" class="form-control">
                                <option value="null" selected>MM</option>
                                @for($i=1;$i<=12;$i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                            <select name="expire_year" id="expire_year" class="form-control">
                                <option value="null" selected>AA</option>
                                @for($i=2019;$i<=2025;$i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">CSV</label>
                            <input type="number" name="csv" class="form-control" id="csv" placeholder="3 digitos">
                        </div>
                        <input type="submit" value="Enviar" class="enviar" onclick="validate();return false">
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
