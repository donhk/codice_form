$(document).ready(function () {
    $('#select-opt option[value="1"]').attr('selected', true);
    $(window).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });
});

function refresh(e) {
    $.ajax({
        url: "/price/" + e.value
    }).done(function (data) {
        var obj = jQuery.parseJSON(data);
        $('#price').val("$" + obj.price + ".00");
    });
}

function validate() {
    let pattern = new RegExp("^[0-9]{16}$");
    let pattern2 = new RegExp("^[0-9]{3}$");
    let pattern3 = new RegExp("^[0-9]+$");
    let pattern4 = new RegExp("^[\\w\\W]+$");
    let card_number = $('#card_number').val();
    let card_holder = $('#card_holder').val();
    let card_surnames = $('#card_surnames').val();
    let expire_month = $('#expire_month').find(":selected").text();
    let expire_year = $('#expire_year').find(":selected").text();
    let csv = $('#csv').val();
    //console.log("Invalid card number " + pattern.test(card_number));
    if (pattern.test(card_number) === false || pattern2.test(csv) === false || pattern3.test(expire_month) === false || pattern3.test(expire_year) === false) {
        alert("Invalid numeric value data in form");
        return;
    }
    if (pattern4.test(card_holder) === false || pattern4.test(card_surnames) === false) {
        alert("Invalid alpha value data in form");
        return;
    }
    $("#my_form").submit();
}
