<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class HomeController extends Controller
{
    private $names = array('1' => 'Birko-Flor /  Negro',
        '2' => 'Piel Clásica / Asphalt',
        '3' => 'Piel Grasa / Habana',
        '4' => 'Piel / Plum',
        '5' => 'Wool Felt / Negro',
        '6' => 'Ramses / Negro',
        '7' => 'Birko-Flor / Plata'
    );

    private $prices = array('1' => '1890',
        '2' => '3290',
        '3' => '2390',
        '4' => '1990',
        '5' => '3490',
        '6' => '1890',
        '7' => '1490');

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!$this->middleware('auth')) {
            return view('home');
        }
        $keys = array_keys($this->names);
        return view('form_view')->with('items', $this->names)->with('keys', $keys);
    }

    public function get_price($item)
    {
        if (array_key_exists($item, $this->names)) {
            return '{"price":"' . $this->prices[$item] . '"}';
        }
        return '{"name":"???"}';
    }

    public function paypal(Request $request)
    {

        \Log::info('Validating data');
        $this->validate($request, [
            'product' => 'required|min:1',
            'card_number' => 'required|digits:16',
            'card_holder' => 'required|alpha',
            'card_surnames' => 'required|alpha',
            'expire_month' => 'required|numeric',
            'expire_year' => 'required|numeric',
            'csv' => 'required|digits:3'
        ]);


        $name = $this->names[$request->product];
        $price = $this->prices[$request->product];
        $card_number = $request->card_number;
        $card_holder = $request->card_holder;
        $card_surnames = $request->card_surnames;
        $expire_month = $request->expire_month;
        if (strlen($expire_month) == 1) {
            $expire_month = '0' . $expire_month;
        }
        $expire_year = substr($request->expire_year, 2, 3);
        $csv = $request->csv;

        \Log::info("txn data $name | $price | $card_number | $card_holder | $card_surnames | $expire_month | $expire_year | $csv");

        $paypal_conf = \Config::get('paypal');
        $api_context = new ApiContext(
            new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret']
            )
        );

        $api_context->setConfig($paypal_conf['settings']);
        \Log::info('Processing txn');

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item1 = new Item();
        $item1->setName($name)
            ->setCurrency('MXN')
            ->setQuantity(1)
            ->setSku("123123")// Similar to `item_number` in Classic API
            ->setPrice($price);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $details = new Details();
        $details->setSubtotal($price);

        $amount = new Amount();
        $amount->setCurrency("MXN")
            ->setTotal($price)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($name)
            ->setInvoiceNumber(uniqid());

        \Log::info('after txn');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::to('success_txn'))->setCancelUrl(\URL::to('failed_txn'));

        \Log::info('after redirects');

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($api_context);
        } catch (\PayPal\Exception\PPConnectionException  $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return view('thanks');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return view('failed_txn');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        \Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            return \Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return view('failed_txn');
    }

    public function success_txn()
    {
        $payment_id = \Session::get('paypal_payment_id');
        return view('thanks')->with('payment_id', $payment_id);
    }

    public function failed_txn()
    {
        return view('failed_txn');
    }
}
