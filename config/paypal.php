<?php
/**
 * Created by PhpStorm.
 * User: donhk
 * Date: 9/2/18
 * Time: 5:41 PM
 */

return [
    'client_id' => env('PAYPAL_CLIENT', ''),
    'secret' => env('PAYPAL_SECRET', ''),
    'settings' => array(
        'mode' => env('PAYPAL_MODE', ''),
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'INFO'
    )
];