<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/price/{item}', 'HomeController@get_price')->name('price');
Route::post('/paypal', 'HomeController@paypal')->name('paypal');
Route::get('/success_txn', 'HomeController@success_txn')->name('success_txn');
Route::get('/failed_txn', 'HomeController@failed_txn')->name('failed_txn');
